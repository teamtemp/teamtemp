class Images {
  static const String LOGO = 'assets/logo.png';
  static const String BOOT_WORLD = 'assets/world_1.png';
  static const String WELCOME_WORLD = 'assets/earth-hands.png';
  static const String FEVER = 'assets/Fever.png';
  static const String COUGH = 'assets/Cough.png';
  static const String SHORT_OF_BREATH = 'assets/SOB.png';
  static const String FEELING_ILL = 'assets/Feeling Ill.png';
  static const String HEADACHE = 'assets/Headache.png';
  static const String BODY_ACHES = 'assets/Body Aches.png';
  static const String ODD_TASTE = 'assets/Taste.png';
  static const String ODD_SMELL = 'assets/Smell.png';
  static const String SNEEZE = 'assets/Sneeze.png';
  static const String SORE_THROAT = 'assets/Sore Throat.png';
  static const String OTHER = 'assets/Other.png';
}
