import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  static const Color primary200 = Color(0xFF54B7C3);
  static const Color accent = Colors.pinkAccent;
  static const Color warn = Colors.amber;
  static const Color error = Colors.red;

  static const Color secondary100 = Color(0xFFDCEAF9);
  static const Color secondary800 = Color(0xFF022628);

  static const Color brandPrimary = primary200;
  static const Color brandSecondary = Color(0xFFCC8700);
  static const Color buttonBackground = Color(0xFFCFD8DC);

  static const Color whiteHighEmphasis = Color(0xFFFFFFFF);
  static const Color whiteMediumEmphasis = Color(0xB3FFFFFF);
  static const Color primaryTextColor = Color(0xFF006064);
  static const Color darkMediumEmphasis = Colors.black54;

  static const Color buttonColorInactive = Color(0xFFCFD8DC);
  static const Color buttonColorSelectedGood = Color(0xFF02BEB2);
  static const Color buttonColorSelectedBad = Color(0xFFE59797);
  static const Color buttonDisabled = Color(0xFF9E9E9E);

  static const Color accentBlue = Color(0xFFDCEAF9);
  static const Color accentGreen = Color(0xFFBCE4EA);

  static const Color googleBtnBackground = Color.fromARGB(255, 66, 133, 244);
  static const Color anonymousBtnBackground = Color.fromARGB(255, 61, 61, 61);
  static const Color modalBackground = Color.fromARGB(255, 0, 0, 0);
}
