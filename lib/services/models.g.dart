// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ModelTest _$ModelTestFromJson(Map<String, dynamic> json) {
  return ModelTest(
    title: json['title'] as String ?? '',
  );
}

Map<String, dynamic> _$ModelTestToJson(ModelTest instance) => <String, dynamic>{
      'title': instance.title,
    };
